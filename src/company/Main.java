package company;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;


public class Main {

    public static void main(String[] args) {

        Place place = new Place();
        place.setName("World");

        Human human = new Human();
        human.setMessage("Hi");
        human.getPlace();

    }

    //serialization and deserialization <Map>
    void MapToJSON() {
        Gson gson = new Gson();

        Map<String, Integer> someData = new LinkedHashMap<>();
        someData.put("USD", 1000);
        someData.put("EUR", 1000);

        String json = gson.toJson(someData);

        //deserialization
        Type type = new TypeToken<Map<String, Integer>>(){}.getType();
        Map<String, Integer> read = gson.fromJson(json, type);


    }

    static class User{
        private String name;

        public User(String name) {
            this.name = name;
        }
    }

    //simple Data bind
    void GenerationJSON() {
        Human human = new Human();

        //to json
        Gson gson = new Gson();
        String jsonString = gson.toJson(human);

        //from json
        Human _human = gson.fromJson(jsonString, Human.class);

    }

    //notSimple TreeModel
    //write
    void WriteJSON() {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty("message", "Hi");
        JsonObject childObject = new JsonObject();
        childObject.addProperty("name", "World!");
        rootObject.add("place", childObject);

        Gson gson = new Gson();
        String json = gson.toJson(rootObject);
        System.out.println(json); // print "{"message":"Hi","place":{"name":"World!"}}"
    }
    //read
    void ReadJSON() {
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse("{\"message\":\"Hi\",\"place\":{\"name\":\"World!\"}}");
        JsonObject rootObject = jsonElement.getAsJsonObject();
        String message = rootObject.get("message").getAsString(); // get property "message"
        JsonObject childObject = rootObject.getAsJsonObject("place"); // get place object
        String place = childObject.get("name").getAsString(); // get property "name"
        System.out.println(message + " " + place); // print "Hi World!"*/

    }

    static class Human {
        private String message;
        private Place place;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Place getPlace() {
            return place;
        }

        public void setPlace(Place place) {
            this.place = place;
        }

        public void say() {
            System.out.println();
            System.out.println(getMessage() + " , " + getPlace().getName() + "!");
        }

    }

    static class Place {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
